class ProductsController < ApplicationController
  before_action :get_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.all
  end

  def new
    # @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @product.update(product_params)
      redirect_to @product
    else
      render 'edit'
    end
  end

  def destroy
    @article = Product.find(params[:id])
    @article.destroy
    redirect_to products_path
  end

  private
  def get_product
    unless (@product = Product.find(params[:id]))
      redirect_to products_path and return
    end
  end

  def product_params
    params.require(:product).permit(:title, :description, :price, :image_url)
  end

end
